﻿using System.Threading.Tasks;
using System.Windows;

namespace voicemeeter_checker
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            Loaded += onLoad;
        }

        private async void onLoad(object sender, RoutedEventArgs e)
        {
            await Task.Run(checkVersions);
        }

        public async void checkVersions()
        {
            var installed = Versions.getInstalled();
            var latest = await Versions.getLatest();

            foreach (var item in installed)
            {
                var product = item.Key;
                var iVersion = item.Value;
                var lVersion = installed[product];
            }
        }
    }
}
