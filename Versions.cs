﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace voicemeeter_checker
{
    public class Versions : Dictionary<string, Version>
    {
        static Dictionary<string, string> products()
        {
            return new Dictionary<string, string>()
            {
                { "VoiceMeeter", "https://vb-audio.com/Voicemeeter/index.htm" },
                { "VoiceMeeter Banana", "https://vb-audio.com/Voicemeeter/banana.htm" },
                { "VoiceMeeter Potato", "https://vb-audio.com/Voicemeeter/potato.htm" }
            };
        }

        static async Task<Version> getVersion(string url)
        {
            var request = WebRequest.Create(url) as HttpWebRequest;
            var response = await request.GetResponseAsync() as HttpWebResponse;

            // TODO: remove
            Task.Delay(1500).Wait();

            if (response.StatusCode != HttpStatusCode.OK) return null;
            
            Stream receiveStream = response.GetResponseStream();

            StreamReader readStream;
            if (String.IsNullOrWhiteSpace(response.CharacterSet))
            {
                readStream = new StreamReader(receiveStream);
            }
            else
            {
                readStream = new StreamReader(receiveStream, Encoding.GetEncoding(response.CharacterSet));
            }

            string data = readStream.ReadToEnd();

            response.Close();
            readStream.Close();

            var regex = new Regex(@"Voicemeeter ([\d.]+)");

            try
            {
                var match = regex.Match(data);
                var version = match.Groups[1].Value;

                return new Version(version);
            }
            catch (Exception)
            {
                return null;
            }
        }

        public static Versions getInstalled()
        {
            var versions = new Versions();
            var products = Versions.products();

            var path = @"C:\Program Files (x86)\VB\Voicemeeter";
            var info = new DirectoryInfo(path);

            foreach (var file in info.EnumerateFiles())
            {
                if (file.Extension != ".exe") continue;

                var fVersion = FileVersionInfo.GetVersionInfo(file.FullName);
                var name = fVersion.ProductName;
                var version = fVersion.ProductVersion.Replace(", ", ".");

                if (!products.ContainsKey(name) || versions.ContainsKey(name)) continue;

                versions.Add(name, new Version(version));
            }

            return versions;
        }

        public static async Task<Versions> getLatest()
        {
            var versions = new Versions();
            var products = Versions.products();

            foreach (var entry in products)
            {
                var version = await getVersion(entry.Value);

                versions.Add(entry.Key, version);
            }

            return versions;
        }
    }
}
